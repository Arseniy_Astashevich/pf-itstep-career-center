package by.itstep.careercenter.ui.dto;

public class VacancyDto {

    private String experience;
    private String company;
    private String position;
    private String description;

    public VacancyDto(String experience, String company, String position, String description) {
        this.experience = experience;
        this.company = company;
        this.position = position;
        this.description = description;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
