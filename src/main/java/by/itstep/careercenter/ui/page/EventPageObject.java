package by.itstep.careercenter.ui.page;

import by.itstep.careercenter.ui.dto.EventDto;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class EventPageObject extends AbstractPageObject {

    @FindBy(xpath = "//div[@class='row section-name']//h1")
    private WebElement title;

    @FindBy(xpath = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']//div")
    private WebElement description;

    @FindBy(xpath = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//p[contains(strong,'Время проведения:')]/span")
    private WebElement date;

    @FindBy(xpath = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//p[contains(strong,'Категория:')]/span")
    private WebElement category;

    @FindBy(xpath = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//p[contains(strong,'Адрес:')]/span")
    private WebElement address;

    @FindBy(xpath = "//div[@class='buttonWrapper']" +
            "//a[contains(text(),'Записаться')]")
    private WebElement registrationFormButton;



    public EventPageObject(WebDriver driver) {
        super(driver);
    }

    public String readTitle() {
        wait.until(elementToBeClickable(title));

        return title.getText();
    }

    public String readDescription() {
        wait.until(elementToBeClickable(description));

        return description.getText();
    }

    public String readDate() {
        wait.until(elementToBeClickable(date));

        return date.getText();
    }

    public String readCategory() {
        wait.until(elementToBeClickable(category));

        return category.getText();
    }

    public String readAddress() {
        wait.until(elementToBeClickable(address));

        return address.getText();
    }

    public EventDto readEvent () {
        return new EventDto(readTitle(), readDescription(), readDate(), readCategory(), readAddress());
    }

    public void openRegistrationForm() {
        wait.until(elementToBeClickable(registrationFormButton));

        registrationFormButton.click();
    }
}
