package by.itstep.careercenter.ui.page;

import by.itstep.careercenter.constants.CareerCenterConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static by.itstep.careercenter.constants.CareerCenterConstants.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class MainPageObject extends AbstractPageObject {

    private static final String CAREER_CENTER = HOST;

    @FindBy(xpath = "//div[@class='row events-card-wrapper']//a")
    private WebElement event;

    @FindBy(xpath = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть все мероприятия')]")
    private WebElement allEvents;

    @FindBy(xpath = "//div[@class='row main-project']" +
            "//img[@class='image-project-main')]")
    private WebElement project;

    @FindBy(xpath = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть все проекты')]")
    private WebElement allProjects;

    @FindBy(xpath = "//div[@class='row stories-container']" +
            "//a[@class='storiesModalOpen')]")
    private WebElement story;

    @FindBy(xpath = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть еще истории ')]")
    private WebElement allStories;

    @FindBy(xpath = "//div[@class='container-fluid vacancy-wrapper']" +
            "//a[@class='vacancy-box']")
    private WebElement vacancy;

    @FindBy(xpath = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть еще вакансии ')]")
    private WebElement allVacancies;

    @FindBy(xpath = "//div[@id='myFeedback']//div[@class='feedback resume ']" +
            "//a[contains(text(),'Заполнить резюме')]")
    private WebElement cvForm;

    @FindBy(xpath = "//div[@id='myFeedback']//div[@class='feedback consult']" +
            "//a[contains(text(),'Получить консультацию')]")
    private WebElement getConsultation;



    public MainPageObject(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(CAREER_CENTER);
    }

    public void openEvent() {
        wait.until(elementToBeClickable(event));

        event.click();
    }

    public void openAllEventsPage() {
        wait.until(elementToBeClickable(allEvents));

        allEvents.click();
    }

    public void openProject() {
       wait.until(elementToBeClickable(project));

        project.click();
    }

    public void openAllProjectPage() {
        wait.until(elementToBeClickable(allProjects));

        allProjects.click();
    }

    public void openStoryPopup() {
        wait.until(elementToBeClickable(story));

        story.click();
    }

    public void openAllStoriesPage() {
        wait.until(elementToBeClickable(allStories));

        allStories.click();
    }

    public VacancyModalWindow openVacancyPopup() {
        wait.until(elementToBeClickable(vacancy));

        vacancy.click();
        return new VacancyModalWindow(driver);
    }

    public void openAllVacanciesPage() {
        wait.until(elementToBeClickable(allVacancies));

        allVacancies.click();
    }

    public void openCvFormPage() {
        wait.until(elementToBeClickable(cvForm));

        cvForm.click();
    }

    public void openGetConsultationPopup() {
        wait.until(elementToBeClickable(getConsultation));

        getConsultation.click();
    }

}
