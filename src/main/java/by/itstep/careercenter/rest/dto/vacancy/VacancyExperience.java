package by.itstep.careercenter.rest.dto.vacancy;

public enum VacancyExperience {

    INTERN, JUNIOR, MIDDLE, SENIOR

}
