package by.itstep.careercenter.rest.dto.vacancy;

import java.util.Objects;

public class VacancyCreationRequest {

    private String name;
    private String description;
    private String company;
    private VacancyExperience experience;

    public VacancyCreationRequest() {
    }

    public VacancyCreationRequest(String name, String description, String company, VacancyExperience experience) {
        this.name = name;
        this.description = description;
        this.company = company;
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public VacancyExperience getExperience() {
        return experience;
    }

    public void setExperience(VacancyExperience experience) {
        this.experience = experience;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VacancyCreationRequest that = (VacancyCreationRequest) o;
        return Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(company, that.company) && experience == that.experience;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, company, experience);
    }

    @Override
    public String toString() {
        return "VacancyCreationRequest{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", company='" + company + '\'' +
                ", experience=" + experience +
                '}';
    }

    public String toJson() {
        String json = "{\n" +
                "  \"company\": \"" + this.company + "\",\n" +
                "  \"description\": \"" + this.description + "\",\n" +
                "  \"experience\": \"" + this.experience + "\",\n" +
                "  \"name\": \"" + this.name + "\" }";

        return json;
    }
}
