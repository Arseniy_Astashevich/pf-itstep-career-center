package by.itstep.careercenter.rest.dto.vacancy;

import io.restassured.path.json.JsonPath;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VacancyPreview {

    private String id;
    private String name;
    private String company;
    private VacancyExperience experience;

    public VacancyPreview() {
    }

    public VacancyPreview(String id, String name, String company, VacancyExperience experience) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.experience = experience;
    }

    public static List<VacancyPreview> from(JsonPath jsonPath, int size) {
        List<VacancyPreview> previews = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            VacancyPreview vacancy = new VacancyPreview();


            String idFromJson = jsonPath.getString("[" + i + "]['id']");
            String companyFromJson = jsonPath.getString("[" + i + "]['company']");
            String nameFromJson = jsonPath.getString("[" + i + "]['name']");
            String experienceFromJson = jsonPath.getString("[" + i + "]['experience']");

            VacancyExperience exp = VacancyExperience.valueOf(experienceFromJson);

            vacancy.setId(idFromJson);
            vacancy.setCompany(companyFromJson);
            vacancy.setName(nameFromJson);
            vacancy.setExperience(exp);

            previews.add(vacancy);
        }


        return previews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public VacancyExperience getExperience() {
        return experience;
    }

    public void setExperience(VacancyExperience experience) {
        this.experience = experience;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VacancyPreview that = (VacancyPreview) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(company, that.company) && experience == that.experience;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, company, experience);
    }

    @Override
    public String toString() {
        return "VacancyPreview{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", company='" + company + '\'' +
                ", experience=" + experience +
                '}';
    }
}
