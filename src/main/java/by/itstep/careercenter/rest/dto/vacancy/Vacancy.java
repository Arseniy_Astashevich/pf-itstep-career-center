package by.itstep.careercenter.rest.dto.vacancy;

import io.restassured.path.json.JsonPath;

import java.util.Objects;

public class Vacancy {

    private String id;
    private String name;
    private String description;
    private String company;
    private VacancyExperience experience;
    private String createdAt;

    public Vacancy() {
    }

    public Vacancy(String id, String name, String description, String company, VacancyExperience experience, String createdAt) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.company = company;
        this.experience = experience;
        this.createdAt = createdAt;
    }

    public static Vacancy from(JsonPath jsonPath) {
        Vacancy vacancy = new Vacancy();

        String idFromJson = jsonPath.getString("id");
        String companyFromJson = jsonPath.getString("company");
        String nameFromJson = jsonPath.getString("name");
        String experienceFromJson = jsonPath.getString("experience");
        String createdAtFromJson = jsonPath.getString("createdAt");
        String descriptionFromJson = jsonPath.getString("description");

        VacancyExperience exp = VacancyExperience.valueOf(experienceFromJson);

        vacancy.setId(idFromJson);
        vacancy.setCompany(companyFromJson);
        vacancy.setName(nameFromJson);
        vacancy.setExperience(exp);
        vacancy.setDescription(descriptionFromJson);
        vacancy.setCreatedAt(createdAtFromJson);

        return vacancy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public VacancyExperience getExperience() {
        return experience;
    }

    public void setExperience(VacancyExperience experience) {
        this.experience = experience;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vacancy vacancy = (Vacancy) o;
        return Objects.equals(id, vacancy.id) && Objects.equals(name, vacancy.name) && Objects.equals(description, vacancy.description) && Objects.equals(company, vacancy.company) && experience == vacancy.experience && Objects.equals(createdAt, vacancy.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, company, experience, createdAt);
    }

    @Override
    public String toString() {
        return "Vacancy{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", company='" + company + '\'' +
                ", experience=" + experience +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
