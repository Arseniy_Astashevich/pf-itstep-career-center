package by.itstep.careercenter.rest.dto.vacancy;

import java.util.Objects;

//builder
public class VacancyUpdateRequest {

    private String id;
    private String name;
    private String description;
    private String company;
    private VacancyExperience experience;

    public VacancyUpdateRequest() {
    }

    public VacancyUpdateRequest(String id, String name, String description, String company, VacancyExperience experience) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.company = company;
        this.experience = experience;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public VacancyExperience getExperience() {
        return experience;
    }

    public void setExperience(VacancyExperience experience) {
        this.experience = experience;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VacancyUpdateRequest that = (VacancyUpdateRequest) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(company, that.company) && experience == that.experience;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, company, experience);
    }

    @Override
    public String toString() {
        return "VacancyUpdateRequest{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", company='" + company + '\'' +
                ", experience=" + experience +
                '}';
    }

    public String toJson() {
        String json = "{\n" +
                "  \"id\": \"" + this.id + "\",\n" +
                "  \"company\": \"" + this.company + "\",\n" +
                "  \"description\": \"" + this.description + "\",\n" +
                "  \"experience\": \"" + this.experience + "\",\n" +
                "  \"name\": \"" + this.name + "\" }";

        return json;
    }
}
