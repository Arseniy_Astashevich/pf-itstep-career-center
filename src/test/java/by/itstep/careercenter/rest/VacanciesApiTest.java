package by.itstep.careercenter.rest;

import by.itstep.careercenter.rest.dto.vacancy.Vacancy;
import by.itstep.careercenter.rest.dto.vacancy.VacancyCreationRequest;
import by.itstep.careercenter.rest.dto.vacancy.VacancyExperience;
import by.itstep.careercenter.rest.dto.vacancy.VacancyUpdateRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class VacanciesApiTest extends AbstractApiTest{

    @Test
    public void testFindById_happyPass() {
        //given
        RequestSpecification httpRequest = RestAssured.given();
        String existingId = "f75cbdf3-e45f-4ec5-ae40-a6634848db5a";

        //when
        Response response = httpRequest.request(Method.GET, "/api/v1/vacancies/" + existingId);

        //then
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
        System.out.println("==== Response ====");
        System.out.println(response.getBody().asString());
    }

    @Test
    public void testFindById_whenNotExistingId() {
        //given
        RequestSpecification httpRequest = RestAssured.given();
        String notExistingId = UUID.randomUUID().toString();

        //when
        Response response = httpRequest.request(Method.GET, "/api/v1/vacancies/" + notExistingId);

        //then
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 404);
        System.out.println("==== Response ====");
        System.out.println(response.getBody().asString());
    }

    @Test
    public void testFindAll_happyPath() {
        //given
        RequestSpecification httpRequest = RestAssured.given();


        //when
        Response response = httpRequest.request(Method.GET, "/api/v1/vacancies?page=0&size=3");

        //then
        Assert.assertEquals(response.getStatusCode(), 200);
        System.out.println("==== Response ====");
        System.out.println(response.getBody().asString());
    }

    @Test
    public void testFindAll_whenPageAndSizeAreNotSpecified() {
        //given
        RequestSpecification httpRequest = RestAssured.given();


        //when
        Response response = httpRequest.request(Method.GET, "/api/v1/vacancies");

        //then
        Assert.assertEquals(response.getStatusCode(), 400);
    }

    @Test
    public void testDelete_happyPath() {
        //given
        String idToDelete = createVacancyAndGetId();

        RequestSpecification httpRequest = getSpecificationWithToken();

        //when
        Response response = httpRequest.request(Method.DELETE, "/api/v1/vacancies/" + idToDelete);

        //then
        Assert.assertEquals(response.getStatusCode(), 204);

        assertThatVacancyDoesDoesNotExist(idToDelete);
    }

    @Test
    public void testDelete_whenNotAuthorized() {
        //given
        RequestSpecification httpRequest = RestAssured.given();
        String existingId = "f75cbdf3-e45f-4ec5-ae40-a6634848db5a";

        //when
        Response response = httpRequest.request(Method.DELETE, "/api/v1/vacancies/" + existingId);


        //then
        Assert.assertEquals(response.getStatusCode(), 403);
    }

    @Test
    public void testLogin_happyPath() {
        //given
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        httpRequest.body("{\n" +
                "  \"password\": \"admin\",\n" +
                "  \"username\": \"admin\"\n" +
                "}");


        //when
        Response response = httpRequest.request(Method.POST, "/api/v1/auth/login");


        //then
        Assert.assertEquals(response.getStatusCode(), 200);
        System.out.println("==== LOGIN RESPONSE ====");
        System.out.println(response.getBody().asString());

    }

    @Test
    public void create_happyPath() {
        //given
        String token = getToken();

        RequestSpecification httpRequest = RestAssured.given();

        VacancyCreationRequest request = new VacancyCreationRequest("Java developer",
                "Middle Java Developer",
                "Bobson Technologies",
                VacancyExperience.MIDDLE);
        String jsonRequest = request.toJson();

        httpRequest.body(jsonRequest);
        httpRequest.header("Content-Type", "application/json");
        httpRequest.header("Authorization", "Bearer_" + token);

        //when
        Response response = httpRequest.request(Method.POST, "/api/v1/vacancies");

        //then
        Assert.assertEquals(response.getStatusCode(), 201);

        JsonPath jsonPath = response.body().jsonPath();
        Vacancy createdVacancy = Vacancy.from(jsonPath);

        Assert.assertEquals(
                createdVacancy.getName(),
                request.getName());

        Assert.assertEquals(
                createdVacancy.getDescription(),
                request.getDescription());

        removeVacancy(createdVacancy.getId());

    }

    @Test
    public void create_whenNameIsEmpty() {
        //given

        RequestSpecification httpRequest = getSpecificationWithToken();

        httpRequest.body("{\n" +
                "  \"company\": \"Bobson Technologies\",\n" +
                "  \"description\": \"Middle Java Developer\",\n" +
                "  \"experience\": \"JUNIOR\"\n" +
                "}");
        httpRequest.contentType(ContentType.JSON);

        //when
        Response response = httpRequest.request(Method.POST, "/api/v1/vacancies");

        //then
        Assert.assertEquals(response.getStatusCode(), 400);
        System.out.println(response.getBody().asString());
    }

    @Test
    public void update_happyPath() {
        //given
        String existingId = createVacancyAndGetId();
        RequestSpecification httpRequest = getSpecificationWithToken();
        httpRequest.contentType(ContentType.JSON);

        VacancyUpdateRequest updateRequest = new VacancyUpdateRequest(
                existingId,
                "updated_vacancy",
                "updated_description",
                "updated_Bobson Technologies",
                VacancyExperience.SENIOR
        );

        httpRequest.body(updateRequest.toJson());


        //when
        Response response = httpRequest.request(Method.PUT, "/api/v1/vacancies");

        //then
        Assert.assertEquals(response.getStatusCode(), 200);

        JsonPath jsonPath = response.getBody().jsonPath();
        Vacancy updatedVacancy = Vacancy.from(jsonPath);

        // ... assert equals for each field
        Assert.assertEquals(
                updatedVacancy.getName(),
                updateRequest.getName());
        Assert.assertNotNull(updatedVacancy.getCreatedAt());

        removeVacancy(existingId);

    }



    public void removeVacancy(String existingId) {
        getSpecificationWithToken()
                .request(Method.DELETE, "/api/v1/vacancies/" + existingId);
    }

    public String createVacancyAndGetId() {
        RequestSpecification httpRequest = getSpecificationWithToken();
        httpRequest.contentType(ContentType.JSON);
        httpRequest.body("{\n" +
                "  \"company\": \"Bobson Technologies\",\n" +
                "  \"description\": \"Middle Java Developer\",\n" +
                "  \"experience\": \"JUNIOR\",\n" +
                "  \"name\": \"Java developer\"\n" +
                "}");

        String id = httpRequest
                .request(Method.POST, "/api/v1/vacancies")
                .body()
                .jsonPath()
                .getString("id");

        return id;
    }

    public boolean assertThatVacancyDoesDoesNotExist(String id) {
        RequestSpecification httpRequest = getSpecificationWithToken();
        int statusCode = httpRequest
                .request(Method.GET, "/api/v1/vacancies/" + id)
                .getStatusCode();

        return statusCode == 404;
    }
}
