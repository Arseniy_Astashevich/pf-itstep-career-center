package by.itstep.careercenter.ui;

import by.itstep.careercenter.ui.page.EventPageObject;
import by.itstep.careercenter.ui.page.MainPageObject;
import by.itstep.careercenter.ui.page.VacancyModalWindow;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EventPageTest extends AbstractUiTest {

    @Test
    public void open() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();

        //when
        mainPage.openEvent();

        //then
        Assert.assertTrue(driver.getCurrentUrl().contains("/events/"));
    }

    @Test
    public void readTitle_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();

        //when
        mainPage.openEvent();
        EventPageObject eventPage = PageFactory.initElements(driver, EventPageObject.class);

        //then
        System.out.println(eventPage.readTitle());
        Assert.assertNotNull(eventPage.readTitle());
    }

    @Test
    public void readDescription_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();

        //when
        mainPage.openEvent();
        EventPageObject eventPage = PageFactory.initElements(driver, EventPageObject.class);

        //then
        System.out.println(eventPage.readDescription());
        Assert.assertNotNull(eventPage.readDescription());
    }

    @Test
    public void readDate_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();

        //when
        mainPage.openEvent();
        EventPageObject eventPage = PageFactory.initElements(driver, EventPageObject.class);

        //then
        System.out.println(eventPage.readDate());
        Assert.assertNotNull(eventPage.readDate());
    }

    @Test
    public void readCategory_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();

        //when
        mainPage.openEvent();
        EventPageObject eventPage = PageFactory.initElements(driver, EventPageObject.class);

        //then
        System.out.println(eventPage.readCategory());
        Assert.assertNotNull(eventPage.readCategory());
    }
}
