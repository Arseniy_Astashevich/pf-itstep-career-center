package by.itstep.careercenter.ui;

import by.itstep.careercenter.rest.dto.vacancy.VacancyExperience;
import by.itstep.careercenter.ui.page.MainPageObject;
import by.itstep.careercenter.ui.page.VacancyModalWindow;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VacancyModalWindowTest extends AbstractUiTest{

    @Test
    public void open_when_vacancyPopupHasOpened() {
    //given
    MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
    mainPage.open();

    //when
    mainPage.openVacancyPopup();
    VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);


    //then
    Assert.assertTrue(vacancyModalWindow.checkVisibility());
    }

    @Test
    public void readExperience_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();
        mainPage.openVacancyPopup();

        //when
        VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);

        //then
        System.out.println(vacancyModalWindow.readExperience());
        Assert.assertEquals(vacancyModalWindow.readExperience(),
                VacancyExperience.valueOf(vacancyModalWindow.readExperience()).toString());
    }

    @Test
    public void readCompany_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();
        mainPage.openVacancyPopup();

        //when
        VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);

        //then
        System.out.println(vacancyModalWindow.readExperience());
        Assert.assertNotNull(vacancyModalWindow.readCompany());
    }

    @Test
    public void readPosition_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();
        mainPage.openVacancyPopup();

        //when
        VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);

        //then
        System.out.println(vacancyModalWindow.readExperience());
        Assert.assertNotNull(vacancyModalWindow.readPosition());
    }

    @Test
    public void closeVacancyPopup_happyPath() {
        //given
        MainPageObject mainPage = PageFactory.initElements(driver, MainPageObject.class);
        mainPage.open();
        mainPage.openVacancyPopup();

        //when
        VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);
        vacancyModalWindow.closeVacancyPopup_byButton();

        //then
        Assert.assertTrue(vacancyModalWindow.checkInvisibility());
    }



}
